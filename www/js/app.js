// ionic 1.7.12
// node v4.2.6

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

CONST_PRODUCTION = {
    "BASE_SERVER_URL_SECURED": "https://app.zclassapp.com/api/",
    "BASE_SERVER_API_URL_SECURED": "https://app.zclassapp.com/api/",
    "BASE_SERVER_URL": "https://app.zclassapp.com/"
};
CONST_DEVELOPMENT = {
    "BASE_SERVER_URL_SECURED": "http://localhost:3000/",
    "BASE_SERVER_API_URL_SECURED": "http://localhost:3000/api/",
    "BASE_SERVER_URL": "http://localhost:3000/"
};



angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngActionCable'])

.constant("constants", CONST_PRODUCTION)


.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})




.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.backButton.text('').icon('ion-ios7-arrow-left').previousTitleText(false);
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.tabs.style('standard');

    $ionicConfigProvider.views.maxCache(0);

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

        .state('begin', {
        url: '/begin',
        templateUrl: 'templates/begin.html'
    })

    .state('select-school', {
        url: '/select-school',
        templateUrl: 'templates/select-school.html'
    })

    .state('select-profile', {
        url: '/select-profile',
        templateUrl: 'templates/select-profile.html'
    })


    .state('profile-login', {
        url: '/profile-login',
        templateUrl: 'templates/profile-login.html'
    })

    /*.state('forgot-password', {
        url: '/forgot-password',
        templateUrl: 'templates/forgot-password.html'
    })*/


    // setup an abstract state for the tabs directive
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })

    // Each tab has its own nav history stack:

    .state('tab.feed', {
        url: '/feed',
        views: {
            'tab-feed': {
                templateUrl: 'templates/tab-feed.html'
            }
        }
    })

    .state('tab.feed-news', {
        url: '/feed/feed-news/:new_feed_id',
        views: {
            'tab-feed': {
                templateUrl: 'templates/explore-news.html'
            }
        }
    })


    .state('tab.activitys', {
        url: '/activitys',
        views: {
            'tab-activitys': {
                templateUrl: 'templates/tab-activitys.html'
            }
        }
    })

    .state('tab.activity-question', {
        url: '/activitys/:school_activity_id',
        views: {
            'tab-activitys': {
                templateUrl: 'templates/activity-question.html'
            }
        }
    })

    .state('tab.activity-answer', {
        url: '/activitys/activity-answer/:school_activity_id/:question_id',
        views: {
            'tab-activitys': {
                templateUrl: 'templates/activity-answer.html'
            }
        }
    })


    .state('tab.explore', {
        url: '/explore',
        views: {
            'tab-explore': {
                templateUrl: 'templates/tab-explore.html'
            }
        }
    })

    .state('tab.explore-list', {
        url: '/explore/explore-list/:category_feed_id',
        views: {
            'tab-explore': {
                templateUrl: 'templates/explore-list.html'
            }
        }
    })

    .state('tab.explore-news', {
        url: '/explore/explore-news/:new_feed_id',
        views: {
            'tab-explore': {
                templateUrl: 'templates/explore-news.html'
            }
        }
    })

    .state('tab.account', {
        url: '/account',
        views: {
            'tab-account': {
                templateUrl: 'templates/tab-account.html'
            }
        }
    })

    .state('tab.boletim', {
        url: '/account/boletim',
        views: {
            'tab-account': {
                templateUrl: 'templates/boletim.html'
            }
        }
    })

    .state('tab.boletim-discipline', {
        url: '/account/boletim/boletim-discipline/:discipline_id',
        views: {
            'tab-account': {
                templateUrl: 'templates/boletim-discipline.html'
            }
        }
    })

    .state('tab.mytests', {
        url: '/account/mytests',
        views: {
            'tab-account': {
                templateUrl: 'templates/mytests.html'
            }
        }
    })

    .state('tab.mytests-discipline', {
        url: '/account/mytests/mytests-discipline/:discipline_id',
        views: {
            'tab-account': {
                templateUrl: 'templates/mytests-discipline.html'
            }
        }
    })

    .state('tab.mytests-discipline-info', {
        url: '/account/mytests/mytests-discipline/mytests-discipline-info/:discipline_id/:period_id',
        views: {
            'tab-account': {
                templateUrl: 'templates/mytests-discipline-info.html'
            }
        }
    })

    .state('tab.profile', {
        url: '/account/profile',
        views: {
            'tab-account': {
                templateUrl: 'templates/profile.html'
            }
        }
    })

    .state('tab.teachers', {
        url: '/account/teachers',
        views: {
            'tab-account': {
                templateUrl: 'templates/teachers.html'
            }
        }
    })

    .state('tab.teachers-item', {
        url: '/account/teachers/:discipline_id',
        views: {
            'tab-account': {
                templateUrl: 'templates/teachers-item.html'
            }
        }
    })

    .state('tab.calendar', {
        url: '/account/calendar',
        views: {
            'tab-account': {
                templateUrl: 'templates/calendar.html'
            }
        }
    })

    .state('tab.calendar-month', {
        url: '/account/calendar/:month',
        views: {
            'tab-account': {
                templateUrl: 'templates/calendar-month.html'
            }
        }
    })

    .state('tab.events', {
        url: '/account/events',
        views: {
            'tab-account': {
                templateUrl: 'templates/events.html'
            }
        }
    })

    .state('tab.events-item', {
        url: '/account/events/events-item/:event_id',
        views: {
            'tab-account': {
                templateUrl: 'templates/events-item.html'
            }
        }
    })

    .state('tab.chats', {
        url: '/chats',
        views: {
            'tab-chats': {
                templateUrl: 'templates/tab-chats.html'
            }
        }
    })

    .state('tab.chats-view', {
        url: '/chats/chats-view/:type_user',
        views: {
            'tab-chats': {
                templateUrl: 'templates/chats-view.html'
            }
        }
    })





    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/begin');

});