angular.module('starter.services', [])


.factory('EndPoint', ['$http', 'constants', function($http, constants) {
    return {
        getResource: function(url, params){
            config = {};
            config.params = {};
            for(var key in params) {
                config.params[key] = params[key];
            }
            //config.headers = {'Authorization': token};
            return $http.get(constants.BASE_SERVER_API_URL_SECURED + url, config);
        },
        postResource: function(url, params){
            config = {};
            for(var key in params) {
                config[key] = params[key];
            }
            return $http.post(constants.BASE_SERVER_API_URL_SECURED + url, config);
        }
    };
}])


.factory('Util', function(){
    return {
        empty: function(element){
            flag = true;
            if (element!=null && element!='' && typeof element!=="undefined" && Object.keys(element).length!=0){
                flag = false;
            }
            return flag;
        },
        setSessionStorage: function(key, value){
            window.sessionStorage.setItem(key, JSON.stringify(value));
        },
        getSessionStorage: function(key){
            return JSON.parse(window.sessionStorage.getItem(key) || '{}');
        },
        removeSessionStorage: function(key){
            window.sessionStorage.removeItem(key);
        },
        setlocalStorage: function(key, value){
            window.localStorage.setItem(key, JSON.stringify(value));
        },
        getlocalStorage: function(key){
            return JSON.parse(window.localStorage.getItem(key) || '{}');
        },
        removelocalStorage: function(key){
            window.localStorage.removeItem(key);
        },
        positionObjectActivity: function(activity_id){
            var positionActivity = -1;
            var myObjec = this.getSessionStorage('answers');
            if(!this.empty(myObjec)){
                for(var i=0;i<myObjec.activits.length;i++){
                    if(parseInt(myObjec.activits[i].id)==parseInt(activity_id)){
                        positionActivity = i;
                        break;
                    }
                }
            }
            return positionActivity;
        },
        positionObjectQuestion: function(activity_id, question_id){
            var myObjec = this.getSessionStorage('answers');
            var position = this.positionObjectActivity(activity_id);
            var positionQuestion = -1;
            if(position!=-1){
                for(var i=0;i<myObjec.activits[position].questions.length;i++){
                    if(parseInt(myObjec.activits[position].questions[i].id)==parseInt(question_id)){
                        positionQuestion = i;
                        break;
                    }
                }
            }
            return positionQuestion;
        },
        getAnswerQuestion: function(activity_id, question_id){
            var myObjec = this.getSessionStorage('answers');
            var position_activity = -1;
            var position_question = -1;
            var answer_id = -1;
            if(!this.empty(myObjec)){
                position_activity = this.positionObjectActivity(activity_id);
                position_question = this.positionObjectQuestion(activity_id, question_id);
            }
            if(position_question!=-1){
                answer_id = myObjec.activits[position_activity].questions[position_question].answer_id
            }
            return parseInt(answer_id);
        },
        existObjectAnswerQuestion: function(activity_id, question_id){
            var myObjec = this.getSessionStorage('answers');
            var position_question = -1;
            if(!this.empty(myObjec)){
                position_question = this.positionObjectQuestion(activity_id, question_id);
            }
            return parseInt(position_question);
        },
        setObjectAnswersQuestion: function(activity_id, question_id, answer_id, flag_answer){
            var myObjec = this.getSessionStorage('answers');

            activity_id = parseInt(activity_id);
            question_id = parseInt(question_id);
            answer_id = parseInt(answer_id);
            flag_answer = (flag_answer === 'true');
            
            if(this.empty(myObjec)){
                myObjec = {activits:[]};
                myObjec.activits = [{id:activity_id, questions:[{id:question_id, answer_id:answer_id, flag_answer:flag_answer}]}]
            }else{
                positionActivity = this.positionObjectActivity(activity_id);
                if(positionActivity!=-1){
                    positionQuestionActivity = this.positionObjectQuestion(activity_id, question_id);
                    if(positionQuestionActivity!=-1){
                        myObjec.activits[positionActivity].questions[positionQuestionActivity].answer_id = answer_id;
                        myObjec.activits[positionActivity].questions[positionQuestionActivity].flag_answer = flag_answer;
                    }else{
                        myObjec.activits[positionActivity].questions.push({id:question_id, answer_id:answer_id, flag_answer:flag_answer})
                    }
                }else{
                    myObjec.activits.push({id:activity_id, questions:[{id:question_id, answer_id:answer_id, flag_answer:flag_answer}]})
                }
            }
            this.setSessionStorage("answers", myObjec);
        },
        month: function(month){
            mes = ''
            if(month=='january'){
                mes = 'Janeiro';
            }else if(month=='february'){
                mes = 'Fevereiro';
            }else if(month=='march'){
                mes = 'Março';
            }else if(month=='april'){
                mes = 'Abril';
            }else if(month=='may'){
                mes = 'Maio';
            }else if(month=='june'){
                mes = 'Junho';
            }else if(month=='july'){
                mes = 'Julho';
            }else if(month=='augusty'){
                mes = 'Agosto';
            }else if(month=='september'){
                mes = 'Setembro';
            }else if(month=='october'){
                mes = 'Outubro';
            }else if(month=='november'){
                mes = 'Novembro';
            }else if(month=='december'){
                mes = 'Dezembro';
            }
            return mes;
        }
    };
})




.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
