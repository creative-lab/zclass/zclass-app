angular.module('starter.controllers', [])


/////////////////////////////////////////////////////////////////////////////
////////////////////////////// AUTENTICAÇÃO /////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

.controller('startAppCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.hide();
    if(!Util.empty(Util.getlocalStorage('headers')) && !Util.empty(Util.getlocalStorage('user'))){
        $state.go('tab.feed');
    }    
})

.controller('selectSchoolCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});
    Util.removeSessionStorage('user');
    Util.removelocalStorage('user');
    Util.removelocalStorage('headers');
    $scope.schools = {};
    $scope.dataAccess = {
        school_id: null,
        type_profile: null
    }

    // LISTA DE ESCOLAS
    promise = EndPoint.getResource("v1/schools");
    promise.then(function(response) {
        $scope.schools = response.data;
        $ionicLoading.hide();
    }, function(error) {

    });

    // SELECIONO UMA ESCOLA
    $scope.selectSchool = function(){
        if(!Util.empty($scope.dataAccess.school_id)){
            Util.setSessionStorage('user', $scope.dataAccess);
            $state.go("select-profile");
        }else{
            $ionicPopup.alert({
                title: 'Alert',
                template: '<center>Selecione uma <b>Escola</b></center>',
                buttons: [{
                    text: 'ok',
                    type: 'button-positive',
                }]
            });
        }
    }

    $scope.myGoBack = function(){
        $state.go("begin");
    }
})

.controller('selectProfileCtrl', function($scope, EndPoint, $ionicPopup, $state, Util){
    Util.removelocalStorage('user');
    Util.removelocalStorage('headers');
    $scope.dataAccess = Util.getSessionStorage('user');

    if(Util.empty($scope.dataAccess.school_id)){
        $state.go("select-school");
    }

    $scope.selectTypeProfile = function(type){
        if(type=='student_account'){
            $scope.dataAccess.type_profile = 'student_account';
            Util.setSessionStorage('user', $scope.dataAccess)
        }else if(type=='parent_account'){
            $scope.dataAccess.type_profile = 'parent_account';
            Util.setSessionStorage('user', $scope.dataAccess)
        }
        window.sessionStorage.user = JSON.stringify($scope.dataAccess);
        $state.go("profile-login");
    }

    $scope.myGoBack = function(){
        $state.go("select-school");
    }
})

.controller('loginCtrl', function($scope, $ionicHistory, EndPoint, Util, $ionicPopup, $state, $ionicLoading) {
    $scope.dataAccess = Util.getSessionStorage('user');
    if(Util.empty($scope.dataAccess.type_profile)){
        $state.go("select-profile");
    }

    $scope.user = {};

    $scope.myGoBack = function(){
        $state.go("select-profile");
    }


    $scope.login = function(){            
        $ionicLoading.show({template: 'Aguarde...'});

        if(Util.getSessionStorage('user').type_profile=="student_account"){

            promise = EndPoint.postResource('v1/students/auth/sign_in', {enrollment:$scope.user.enrollment, password:$scope.user.password});
            promise.then(function(response) {

                Util.setlocalStorage('headers', response.headers());           

                promise = EndPoint.getResource("v1/students/student_accounts/"+response.data.data.id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                promise.then(function(response2) {

                    //console.log(response2.data)

                    if(Util.getSessionStorage('user').school_id!=response2.data.school.id){
                        Util.removelocalStorage('user');
                        Util.removelocalStorage('headers');
                        $ionicPopup.alert({
                            title: 'Alert',
                            template: '<center>Matrícula ou senha inválida</center>',
                            buttons: [{
                                text: 'ok',
                                type: 'button-positive',
                            }]
                        });
                        $ionicLoading.hide();
                        $state.go('begin');
                    }else{
                        response2.data.type_profile = Util.getSessionStorage('user').type_profile;
                        Util.removeSessionStorage('answer');
                        Util.setlocalStorage('user', response2.data);
                        $ionicLoading.hide();
                        $state.go('tab.feed');
                    }

                }, function(response2) {
                    Util.removelocalStorage('user');
                    Util.removelocalStorage('headers');
                    $ionicPopup.alert({
                        title: 'Alert',
                        template: '<center>'+response2.data.errors+'</center>',
                        buttons: [{
                            text: 'ok',
                            type: 'button-positive',
                        }]
                    });
                    $ionicLoading.hide();                    
                });
                
            }, function(response) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicPopup.alert({
                    title: 'Alert',
                    template: '<center>'+response.data.errors+'</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $ionicLoading.hide();           
            });

        }else if(Util.getSessionStorage('user').type_profile=='parent_account'){


            promise = EndPoint.postResource('v1/get_parent', {'school_id': Util.getSessionStorage('user').school_id, 'enrollment':$scope.user.enrollment, 'password':$scope.user.password});
            promise.then(function(response) {

                if(response.data){

                    
                    promise = EndPoint.postResource('v1/parents/auth/sign_in', {email:response.data.email, password:$scope.user.password});
                    promise.then(function(response2) {
                        response2.data.school_id = Util.getSessionStorage('user').school_id;
                        
                        Util.setlocalStorage('headers', response2.headers());
                        response2.data.type_profile = Util.getSessionStorage('user').type_profile;
                        response2.data.enrollment_student = $scope.user.enrollment;
                        Util.removeSessionStorage('answer');

                        Util.setlocalStorage('user', response2.data);

                        //console.log(response2.data)

                        $ionicLoading.hide();
                        $state.go('tab.feed');      

                        
                    }, function(response2) {
                        Util.removelocalStorage('user');
                        Util.removelocalStorage('headers');
                        $ionicPopup.alert({
                            title: 'Alert',
                            template: '<center>'+response2.data.errors+'</center>',
                            buttons: [{
                                text: 'ok',
                                type: 'button-positive',
                            }]
                        });
                        $ionicLoading.hide();                    
                    });
                
                }else{

                    $ionicPopup.alert({
                        title: 'Alert',
                        template: '<center>Usuário não encontrado</center>',
                        buttons: [{
                            text: 'ok',
                            type: 'button-positive',
                        }]
                    });
                    $ionicLoading.hide();   

                }
                
            }, function(response) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicPopup.alert({
                    title: 'Alert',
                    template: '<center>'+response.data.errors+'</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $ionicLoading.hide();           
            });



        }else{
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            Util.removeSessionStorage('user');
            $state.go("begin");
        }


    }
})


/////////////////////////////////////////////////////////////////////////
////////////////////////////// STUDENTS / PARENTS /////////////////////////////////
/////////////////////////////////////////////////////////////////////////


// LISTA DE DISCIPLINA DE UM ALUNO
.controller('listDisciplinesCtrl', function($scope, EndPoint, $ionicPopup, $state, Util){
    
    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/disciplines", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.disciplines = response.data;
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/disciplines", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.disciplines = response.data;
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });


    }
})

// LISTA DE DISCIPLINA DE UM ALUNO
.controller('listBoletimCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.disciplines = {};
    $scope.flag_empty = false;
    
    if(Util.getlocalStorage('user').type_profile=="student_account"){
    
        $scope.student = Util.getlocalStorage("user");
        promise = EndPoint.getResource("v1/students/disciplines", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            

            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});



            $scope.disciplines = response.data;
            if(Util.empty($scope.disciplines)){
                $scope.flag_empty = true;
            }else{
                $scope.flag_empty = false;    
            }
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/disciplines", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {

            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});


            $scope.disciplines = response.data;
            
            promise2 = EndPoint.getResource("v1/parents/student_accounts/"+Util.getlocalStorage('user').enrollment_student, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise2.then(function(response) {
                $scope.student = response.data;
                $ionicLoading.hide();                
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $state.go("begin");
            });

        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }
})

.controller('boletimCtrl', function($scope, EndPoint, $stateParams, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.discipline_id = $stateParams.discipline_id;
    $scope.result_disciplines = {};
    $scope.media = 0.0;
    $scope.lacks = 0
    

    if(Util.getlocalStorage('user').type_profile=="student_account"){


        promise = EndPoint.getResource("v1/students/discipline_notes/"+$scope.discipline_id+"/notes", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            if(response.data.length==0){
                $ionicPopup.alert({
                    title: 'Alert',
                    template: '<center>Notas ainda não disponível!!</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $ionicLoading.hide();
                $state.go("tab.boletim");
            }else{
                $scope.result_disciplines = response.data;
                // ordenar
                for(var i=0;i<$scope.result_disciplines.length-1;i++){
                    for(var j=i+1;j<$scope.result_disciplines.length;j++){
                        if($scope.result_disciplines[i].period.id > $scope.result_disciplines[j].period.id){
                            temp = $scope.result_disciplines[i];
                            $scope.result_disciplines[i] = $scope.result_disciplines[j];
                            $scope.result_disciplines[j] = temp;
                        }
                    }
                }
                var soma = 0.0;
                for(var i=0;i<$scope.result_disciplines.length;i++){
                    soma = soma+$scope.result_disciplines[i].note;
                    $scope.lacks = $scope.lacks+$scope.result_disciplines[i].lack
                }
                $scope.media = (soma/$scope.result_disciplines.length).toFixed(2);
                $ionicLoading.hide();
            }
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){


        promise = EndPoint.getResource("v1/parents/discipline_notes/"+$scope.discipline_id+"/notes", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            if(response.data.length==0){
                $ionicPopup.alert({
                    title: 'Alert',
                    template: '<center>Notas ainda não disponível!!</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $ionicLoading.hide();
                $state.go("tab.boletim");
            }else{
                $scope.result_disciplines = response.data;
                // ordenar
                for(var i=0;i<$scope.result_disciplines.length-1;i++){
                    for(var j=i+1;j<$scope.result_disciplines.length;j++){
                        if($scope.result_disciplines[i].period.id > $scope.result_disciplines[j].period.id){
                            temp = $scope.result_disciplines[i];
                            $scope.result_disciplines[i] = $scope.result_disciplines[j];
                            $scope.result_disciplines[j] = temp;
                        }
                    }
                }
                var soma = 0.0;
                for(var i=0;i<$scope.result_disciplines.length;i++){
                    soma = soma+$scope.result_disciplines[i].note;
                    $scope.lacks = $scope.lacks+$scope.result_disciplines[i].lack
                }
                $scope.media = (soma/$scope.result_disciplines.length).toFixed(2);
                $ionicLoading.hide();
            }
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });



    }
})


// LISTA DE FEEDS
.controller('listFeedsCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});
    
    $scope.page = 1;
    $scope.feed_news = [];
    $scope.hasMoreData = true;


    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/new_feeds", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.feed_news = response.data;
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/new_feeds", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.feed_news = response.data;
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }

 
    $scope.doRefresh = function(){
        $ionicLoading.show({template: 'Aguarde...'});
        
        if(Util.getlocalStorage('user').type_profile=="student_account"){

            promise = EndPoint.getResource("v1/students/new_feeds", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.feed_news = response.data;
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

            promise = EndPoint.getResource("v1/parents/new_feeds", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.feed_news = response.data;
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }

    }

    $scope.loadMore = function() {
        $scope.page+= 1;

        if(Util.getlocalStorage('user').type_profile=="student_account"){
            
            promise = EndPoint.getResource("v1/students/new_feeds", {'page':$scope.page, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {

                if (response.data.length==0) {
                    $scope.hasMoreData = false;
                }else{
                    for (i = 0; i < response.data.length; i++) {
                        $scope.feed_news.push(response.data[i]);
                    }
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, function(error) {
                //console.log(error)
            });

        }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

            promise = EndPoint.getResource("v1/parents/new_feeds", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'page':$scope.page, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {

                if (response.data.length==0) {
                    $scope.hasMoreData = false;
                }else{
                    for (i = 0; i < response.data.length; i++) {
                        $scope.feed_news.push(response.data[i]);
                    }
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, function(error) {
                //console.log(error)
            });

        }
    } 
})

// LISTA DE ATIVIDADES
.controller('listActivityCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.school_activities = [];
    $scope.tabDo = true; // setting the first div visible when the page loads
    $scope.tabDone = false; // hidden

    // LISTA DE DISCIPLINAS
    promise = EndPoint.getResource("v1/students/school_activities", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
    promise.then(function(response) {
        $scope.school_activities     = response.data;
        //console.log(response)
        $ionicLoading.hide();
    }, function(error) {
        Util.removelocalStorage('user');
        Util.removelocalStorage('headers');
        $ionicLoading.hide();
        $state.go("begin");
    });

    $scope.showTabDo = function() {
        $scope.tabDo = true;
        $scope.tabDone = false;
    }

    $scope.showTabDone = function() {
        $scope.tabDo = false;
        $scope.tabDone = true;
    }
})

// LISTA DE QUESTÕES DE UMA ATIVIDADE
.controller('listQuestionActivityCtrl', function($stateParams, $scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.school_activity_id = $stateParams.school_activity_id;
    $scope.sendAnswers = false;

    // LISTA DE DISCIPLINAS
    promise = EndPoint.getResource("v1/students/school_activities/"+$scope.school_activity_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
    promise.then(function(response) {
        if(response.data!=null){
            $scope.school_activity     = response.data;
            $scope.funcAllQuestionAnswered();
            
            if($scope.school_activity.flag_answer){
                promise = EndPoint.getResource("v1/students/answer_students/get_result/"+$scope.school_activity.id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                promise.then(function(response) {
                    $scope.result_answer = response.data;
                    $ionicLoading.hide();
                }, function(error) {
                    $state.go("tab.activitys");
                });
            }

        }else{
            $state.go("tab.activitys");
        }
        $ionicLoading.hide();
    }, function(error) {
        Util.removelocalStorage('user');
        Util.removelocalStorage('headers');
        $ionicLoading.hide();
        $state.go("begin");
    });


    $scope.funcBackActivities = function(){
        $state.go("tab.activitys");
    }

    $scope.funcSaveAllAnswer = function(activity_id){
        $ionicLoading.show({template: 'Aguarde... Enviando Respostas...'});

        var myObjec = Util.getSessionStorage('answers');
        position_activity = Util.positionObjectActivity(activity_id);
        element = myObjec.activits[position_activity]
        
        promise = EndPoint.postResource("v1/students/answer_students", {'activity':element, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.result_answer = response.data;
            $ionicLoading.hide();
        }, function(error) {
            $ionicPopup.alert({
                title: 'Erro',
                template: '<center>Ocorreu um erro, tente mais tarde</center>',
                buttons: [{
                    text: 'ok',
                    type: 'button-positive',
                }]
            });
            $state.go("tab.activitys");
            $ionicLoading.hide();
            //Util.removelocalStorage('user');
            //Util.removelocalStorage('headers');
            //$ionicLoading.hide();
            //$state.go("begin");
        });
    }

    $scope.funcAllQuestionAnswered = function(){
        var flag = true;
        for(var i=0; i<$scope.school_activity.questions.length;i++){
            element = Util.existObjectAnswerQuestion($scope.school_activity_id, $scope.school_activity.questions[i].id)
            if (element==-1){
                flag = false;
            }
        }
        if(flag){
            $scope.sendAnswers = true;
        }
    }

    $scope.funcQuestionAnswer = function(activity_id, question_id){
        var flag = false
        element = Util.existObjectAnswerQuestion(activity_id, question_id);
        if(element!=-1){
            flag = true
        }
        //console.log(flag)
        return flag;
    }

    $scope.FlagAnswer = function(activity_id, question_id){
        //alert(question_id)
        //alert(Util.positionObjectQuestionAnwers(activity_id.id, question_id))
    }
})

// LISTA DE OPÇÕES DE RESPOSTA DE UMA QUESTÃO
.controller('listOptionQuestionActivityCtrl', function($stateParams, $scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.answersChoice = {};
    $scope.flag_answer = false;
    $scope.letters = ['A', 'B', 'C', 'D', 'E'];
    $scope.question_id = $stateParams.question_id;
    $scope.school_activity_id = $stateParams.school_activity_id;

    $scope.answersChoice.answersId = Util.getAnswerQuestion($scope.school_activity_id, $scope.question_id);

    // LISTA DE DISCIPLINAS
    promise = EndPoint.getResource("v1/students/questions/"+$scope.question_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
    promise.then(function(response) {
        $scope.question     = response.data;

        //console.log($scope.question)
        
        if($scope.question!=null){
            
            // LISTA DE DISCIPLINAS
            promise = EndPoint.getResource("v1/students/school_activities/"+$scope.question.school_activity_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.school_activity = response.data;

                if($scope.school_activity.flag_answer){
                    Util.setObjectAnswersQuestion($scope.school_activity.id, $scope.question.id, $scope.question.answer_student_id, $scope.school_activity.flag_answer);
                    $scope.answersChoice.answersId = Util.getAnswerQuestion($scope.school_activity_id, $scope.question_id);
                }

                $ionicLoading.hide();
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }else{
            $state.go("tab.activitys");
            $ionicLoading.hide();
        }
    }, function(error) {
        Util.removelocalStorage('user');
        Util.removelocalStorage('headers');
        $ionicLoading.hide();
        $state.go("begin");
    });

    $scope.funcFlagAnswer = function (item) {
        $scope.flag_answer = item.currentTarget.getAttribute("data-correct");
    };

    $scope.funcSaveAnswer = function(){
        if($scope.answersChoice.answersId!=-1){
            Util.setObjectAnswersQuestion($scope.school_activity_id, $scope.question_id, $scope.answersChoice.answersId, $scope.flag_answer);
            $state.go('tab.activity-question', {school_activity_id:$scope.school_activity_id});
        }else{
            var alertPopup = $ionicPopup.alert({
                title: 'Erro',
                template: 'Escolha uma opção!!!'
            });
        }
    }
})



//LISTA DE CATEGORIAS DE UM FEED
.controller('ListCategoryFeedCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.category_feeds = {};

    if(Util.getlocalStorage('user').type_profile=="student_account"){

        // LISTA DE DISCIPLINAS
        promise = EndPoint.getResource("v1/students/category_feeds", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.category_feeds     = response.data;
            //console.log(response.data)
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });


    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        // LISTA DE DISCIPLINAS
        promise = EndPoint.getResource("v1/parents/category_feeds", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.category_feeds     = response.data;
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }

})

// LISTA DE NOTICIAS DE UMA CATEGORIA
.controller('ListCategoryFeedListNewsCtrl', function($stateParams, $scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.category_feed_id = $stateParams.category_feed_id;
    $scope.new_feeds = [];
    $scope.page = 1;
    $scope.hasMoreData = true;
    $scope.flagFeeds = true;


    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/new_feeds/"+$scope.category_feed_id+"/feeds", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.new_feeds     = response.data;
            if($scope.new_feeds.length==0){
                $scope.flagFeeds = false;
            }
            
            promise = EndPoint.getResource("v1/students/category_feeds/"+$scope.category_feed_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response1) {
                $scope.category_feed     = response1.data;
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');

            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/new_feeds/"+$scope.category_feed_id+"/feeds", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.new_feeds     = response.data;
            if($scope.new_feeds.length==0){
                $scope.flagFeeds = false;
            }

            promise = EndPoint.getResource("v1/parents/category_feeds/"+$scope.category_feed_id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response1) {
                $scope.category_feed     = response1.data;
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');

            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });


        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }

    $scope.doRefresh = function(){
        $ionicLoading.show({template: 'Aguarde...'});
        
        if(Util.getlocalStorage('user').type_profile=="student_account"){

            promise = EndPoint.getResource("v1/students/new_feeds/"+$scope.category_feed_id+"/feeds", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.new_feeds     = response.data;
                if($scope.new_feeds.length==0){
                    $scope.flagFeeds = false;
                }
                
                promise = EndPoint.getResource("v1/students/category_feeds/"+$scope.category_feed_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                promise.then(function(response1) {
                    $scope.category_feed     = response1.data;
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');

                }, function(error) {
                    Util.removelocalStorage('user');
                    Util.removelocalStorage('headers');
                    $ionicLoading.hide();
                    $state.go("begin");
                });

            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

            promise = EndPoint.getResource("v1/parents/new_feeds/"+$scope.category_feed_id+"/feeds", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.new_feeds     = response.data;
                if($scope.new_feeds.length==0){
                    $scope.flagFeeds = false;
                }

                promise = EndPoint.getResource("v1/parents/category_feeds/"+$scope.category_feed_id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                promise.then(function(response1) {
                    $scope.category_feed     = response1.data;
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');

                }, function(error) {
                    Util.removelocalStorage('user');
                    Util.removelocalStorage('headers');
                    $ionicLoading.hide();
                    $state.go("begin");
                });


            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }

    }


    $scope.loadMore = function() {
        $scope.page+= 1;

        if(Util.getlocalStorage('user').type_profile=="student_account"){

            promise = EndPoint.getResource("v1/students/new_feeds/"+$scope.category_feed_id+"/feeds", {'page':$scope.page, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {

                if (response.data.length==0) {
                    $scope.hasMoreData = false;
                }else{
                    for (i = 0; i < response.data.length; i++) {
                        $scope.new_feeds.push(response.data[i]);
                    }
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, function(error) {
                //console.log(error)
            });

        }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

            promise = EndPoint.getResource("v1/parents/new_feeds/"+$scope.category_feed_id+"/feeds", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'page':$scope.page, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {

                if (response.data.length==0) {
                    $scope.hasMoreData = false;
                }else{
                    for (i = 0; i < response.data.length; i++) {
                        $scope.new_feeds.push(response.data[i]);
                    }
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }, function(error) {
                //console.log(error)
            });

        }
    };  
})


// NOTICIA ESPECIFICA DE UM FEED
.controller('NewFeedCtrl', function($stateParams, $scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.new_feed_id = $stateParams.new_feed_id;
    $scope.new_feed = {};

    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/new_feeds/"+$scope.new_feed_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.new_feed     = response.data;
            //console.log(response)
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/new_feeds/"+$scope.new_feed_id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.new_feed     = response.data;
            //console.log(response)
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }

})


// MY TESTS
.controller('myTestsCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading, $stateParams){
    // LISTA DE DISCIPLINAS
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.disciplines = {};
    $scope.flag_empty = false;
    
    if(Util.getlocalStorage('user').type_profile=="student_account"){

        $scope.student = Util.getlocalStorage("user");
        promise = EndPoint.getResource("v1/students/disciplines", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.disciplines = response.data;
            if(Util.empty($scope.disciplines)){
                $scope.flag_empty = true;
            }else{
                $scope.flag_empty = false;    
            }
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/disciplines", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.disciplines = response.data;
            
            promise2 = EndPoint.getResource("v1/parents/student_accounts/"+Util.getlocalStorage('user').enrollment_student, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise2.then(function(response) {

                $scope.student = response.data;
                $ionicLoading.hide();
                
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $state.go("begin");
            });



        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }

})

.controller('myTestsDisciplineCtrl', function($scope, EndPoint, $stateParams, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.discipline_id = $stateParams.discipline_id;
    $scope.discipline = {};
    $scope.periods    = {};
    
    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/disciplines/"+$scope.discipline_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.discipline = response.data;
            
            promise = EndPoint.getResource("v1/students/periods", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response2) {
                $scope.periods = response2.data;
                $ionicLoading.hide();
            });

        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/disciplines/"+$scope.discipline_id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.discipline = response.data;
            
            promise = EndPoint.getResource("v1/parents/periods", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response2) {
                $scope.periods = response2.data;
                $ionicLoading.hide();
            });

        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });


    }


})
.controller('myTestsDisciplineTnfoCtrl', function($scope, EndPoint, $stateParams, $ionicPopup, $state, Util, $ionicLoading){
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.discipline_id = $stateParams.discipline_id;
    $scope.period_id     = $stateParams.period_id;
    $scope.result = {};
    
    if(Util.getlocalStorage('user').type_profile=="student_account"){
        
        promise = EndPoint.getResource("v1/students/discipline_notes/"+$scope.discipline_id+"/"+$scope.period_id+"/note", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            if(response.data==null){
                $ionicPopup.alert({
                    title: 'Alert',
                    template: '<center>Nota ainda não disponível!!</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $ionicLoading.hide();
                $state.go("tab.mytests-discipline", {discipline_id:$scope.discipline_id})
            }else{
                $scope.result = response.data;
                $ionicLoading.hide();
            }
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/discipline_notes/"+$scope.discipline_id+"/"+$scope.period_id+"/note", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            if(response.data==null){
                $ionicPopup.alert({
                    title: 'Alert',
                    template: '<center>Nota ainda não disponível!!</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $ionicLoading.hide();
                $state.go("tab.mytests-discipline", {discipline_id:$scope.discipline_id})
            }else{
                $scope.result = response.data;
                $ionicLoading.hide();
            }
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });


    }

})



.controller('activityCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading, $stateParams){
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.school_activity = {};
    $scope.option_answers = [];
    $scope.option_answers = new Array();
    $scope.school_activity_id = $stateParams.school_activity_id;

    promise = EndPoint.getResource("v1/students/school_activities/"+$scope.school_activity_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
    promise.then(function(response) {
        $scope.school_activity     = response.data;
        $ionicLoading.hide();
    }, function(error) {
        Util.removelocalStorage('user');
        Util.removelocalStorage('headers');
        $ionicLoading.hide();
        $state.go("begin");
    });
})


// MATÉRIAS E PROFESSORES
.controller('teacherCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading, $stateParams){
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.disciplines = {};
    $scope.flag_empty = false;

    if(Util.getlocalStorage('user').type_profile=="student_account"){

        $scope.student = Util.getlocalStorage("user");
        promise = EndPoint.getResource("v1/students/disciplines", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            
            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});


            $scope.disciplines = response.data;
            if(Util.empty($scope.disciplines)){
                $scope.flag_empty = true;
            }else{
                $scope.flag_empty = false;    
            }
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/disciplines", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {

            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});


            $scope.disciplines = response.data;
            promise2 = EndPoint.getResource("v1/parents/student_accounts/"+Util.getlocalStorage('user').enrollment_student, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise2.then(function(response) {
                $scope.student = response.data;
                $ionicLoading.hide();
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $state.go("begin");
            });

        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }

})

.controller('teachersItemCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading, $stateParams){
    $ionicLoading.show({template: 'Aguarde...'});
    
    $scope.discipline_id = $stateParams.discipline_id;
    
    if(Util.getlocalStorage('user').type_profile=="student_account"){

        $scope.student = Util.getlocalStorage("user");

        promise = EndPoint.getResource("v1/students/teachers/detail/"+$scope.discipline_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.teacher_detail = response.data;
            promise1 = EndPoint.getResource("v1/students/teachers_class_rooms/schedule/"+$scope.discipline_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise1.then(function(response) {
                $scope.schedule = response.data;
                //console.log(response)
                $ionicLoading.hide();

            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $state.go("begin");
            });

        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){


        promise = EndPoint.getResource("v1/parents/teachers/detail/"+$scope.discipline_id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.teacher_detail = response.data;
            promise1 = EndPoint.getResource("v1/parents/teachers_class_rooms/schedule/"+$scope.discipline_id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise1.then(function(response) {
                $scope.schedule = response.data;
                
                promise2 = EndPoint.getResource("v1/parents/student_accounts/"+Util.getlocalStorage('user').enrollment_student, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                promise2.then(function(response) {

                    $scope.student = response.data;
                    $ionicLoading.hide();
                    
                }, function(error) {
                    Util.removelocalStorage('user');
                    Util.removelocalStorage('headers');
                    $state.go("begin");
                });


            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $state.go("begin");
            });

        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });


    }

})



.controller('profileCtrl', function($scope, EndPoint, $ionicPopup, $state, Util, $ionicLoading, $stateParams, $state){
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.profile = {};
    $scope.flag_password = true;
    $scope.type_profile = Util.getlocalStorage('user').type_profile;
    
    if($scope.type_profile=="student_account"){

        $scope.student = Util.getlocalStorage("user");
        promise = EndPoint.getResource("v1/students/student_accounts/"+$scope.student.id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.profile = response.data;

            if(!Util.empty(response.data.address_city_name)){
                $scope.profile.city_state = response.data.address_city_name+'/'+response.data.address_state_uf;
                $scope.profile.street = response.data.address.street+', '+response.data.address.number+' '+response.data.address.district;
            }else{
                $scope.profile.city_state = '---';   
                $scope.profile.street = '---';
            }
            $ionicLoading.hide();     
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if($scope.type_profile=='parent_account'){

        $scope.parent = Util.getlocalStorage("user").data;
        
        promise = EndPoint.getResource("v1/parents/parent_accounts/"+$scope.parent.id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.profile = response.data;
            

            promise = EndPoint.getResource("v1/parents/parent_accounts/student/"+Util.getlocalStorage('user').enrollment_student, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response1) {

                $scope.profile.enrollment = response1.data.enrollment;      
                $scope.profile.name_student = response1.data.name;
                $scope.profile.class_room = response1.data.class_room;
                $scope.profile.school = response1.data.school;

                $ionicLoading.hide();
            }, function(error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Erro',
                    template: '<center>Ocorreu um erro, tente mais tarde</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
            });

            
        }, function(error) {
            //Util.removelocalStorage('user');
            //Util.removelocalStorage('headers');
            //$state.go("begin");
        });



        $ionicLoading.hide();
    }


    $scope.save = function(){
        if((!Util.empty($scope.profile.password) || !Util.empty($scope.profile.password_confirmation)) && $scope.profile.password!=$scope.profile.password_confirmation){
            $ionicPopup.alert({
                title: 'Erro',
                template: '<center>Senha e confirmação diferentes</center>',
                buttons: [{
                    text: 'ok',
                    type: 'button-positive',
                }]
            });
        }else{
            $ionicLoading.show({template: 'Aguarde...'});
            if($scope.profile.password.length>=6){
                
                if($scope.type_profile=="student_account"){

                    promise = EndPoint.postResource("v1/students/student_accounts/password_edit", {'password': $scope.profile.password, 'password_confirmation':$scope.profile.password_confirmation, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                    promise.then(function(response) {
                        $ionicLoading.hide();
                        $state.go("tab.account");
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Erro',
                            template: '<center>Ocorreu um erro, tente mais tarde</center>',
                            buttons: [{
                                text: 'ok',
                                type: 'button-positive',
                            }]
                        });
                    });

                }else if($scope.type_profile=='parent_account'){

                    promise = EndPoint.postResource("v1/parents/parent_accounts/password_edit", {'password': $scope.profile.password, 'password_confirmation':$scope.profile.password_confirmation, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                    promise.then(function(response) {
                        $ionicLoading.hide();
                        $state.go("tab.account");
                    }, function(error) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Erro',
                            template: '<center>Ocorreu um erro, tente mais tarde</center>',
                            buttons: [{
                                text: 'ok',
                                type: 'button-positive',
                            }]
                        });
                    });
                }


            }else{
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Erro',
                    template: '<center>Senha tem que ser maior que 6 digitos</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
            }
        }
    }

    $scope.password_empty = function(){
        if(!Util.empty($scope.profile.password) || !Util.empty($scope.profile.password_confirmation)){
            $scope.flag_password = false;
        }else{
            $scope.flag_password = true;
        }
    }
})


.controller('accountCtrl', function(EndPoint, $ionicLoading, $scope, Util, $state) {
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.name_menu = 'Aluno';
    if(Util.getlocalStorage('user').type_profile=="parent_account"){
        $scope.name_menu = 'Parente';
    }

    promise = EndPoint.getResource("v1/school-year");
    promise.then(function(response) {
        $scope.site_config = response.data;
        $ionicLoading.hide();
    }, function(error) {});


    $scope.logOff = function(){
        Util.removelocalStorage('user');
        Util.removelocalStorage('headers');
        $state.go("begin");
    }

})

.controller('tabsCtrl', function($scope, Util, $state) {
    if(Util.getlocalStorage('user').type_profile=="student_account"){
        $scope.flag_feed        = true;
        $scope.flag_activity    = true;
        $scope.flag_explore     = true;
        $scope.flag_chat        = true;
        $scope.flag_account     = true;

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){
        $scope.flag_feed        = true;
        $scope.flag_activity    = false;
        $scope.flag_explore     = true;
        $scope.flag_chat        = true;
        $scope.flag_account     = true;
    }
    $scope.routeTab = function(url){
        $state.go(url);
    }
})


.controller('calendarCtrl', function($scope, Util, $state) {
})


.controller('calendarMonthCtrl', function($scope, Util, $state, $ionicLoading, $stateParams, EndPoint, $ionicPopup) {
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.month = $stateParams.month;
    $scope.month_text = Util.month($scope.month);
    $scope.flag_empty = false;

    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/calendars/activity_month", {'month': $scope.month, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {

            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});




            if(!Util.empty(response.data)){
                $scope.activits = response.data;
                $scope.flag_empty = false;    
            }else{
                $scope.flag_empty = true;
            }
            $ionicLoading.hide();
        }, function(error) {
            $ionicLoading.hide();
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/calendars/activity_month", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'month': $scope.month, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {


            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});
            
            if(!Util.empty(response.data)){
                $scope.activits = response.data;
                $scope.month = Util.month(response.data[0].month)
                $scope.flag_empty = false; 
            }else{
                $scope.flag_empty = true; 
            }
            $ionicLoading.hide();
        }, function(error) {
            $ionicLoading.hide();
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $state.go("begin");
        });

    }


})

.controller('eventsCtrl', function($scope, Util, $state, EndPoint, $ionicLoading) {
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.occurrences = [];

    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/occurrences", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {

            $scope.class_room = Util.getlocalStorage('user').class_room;
            
            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});


            $scope.occurrences = response.data;
            //console.log(response.data)
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/occurrences", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            
            promise1 = EndPoint.getResource("v1/school-year");
            promise1.then(function(response1) {
                //console.log(response1)
                $scope.site_config = response1.data;
                $ionicLoading.hide();
            }, function(error) {});

            $scope.occurrences = response.data;
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }


    $scope.doRefresh = function(){
        $ionicLoading.show({template: 'Aguarde...'});
        
        if(Util.getlocalStorage('user').type_profile=="student_account"){

            promise = EndPoint.getResource("v1/students/occurrences", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.occurrences = response.data;
                $ionicLoading.hide();
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

            promise = EndPoint.getResource("v1/parents/occurrences", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.occurrences = response.data;
                $ionicLoading.hide();
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }

    }

    $scope.loadMore = function() {
        $scope.page+= 1;

        if(Util.getlocalStorage('user').type_profile=="student_account"){

            promise = EndPoint.getResource("v1/students/occurrences", {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.occurrences = response.data;
                $ionicLoading.hide();
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

            promise = EndPoint.getResource("v1/parents/occurrences", {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response) {
                $scope.occurrences = response.data;
                $ionicLoading.hide();
            }, function(error) {
                Util.removelocalStorage('user');
                Util.removelocalStorage('headers');
                $ionicLoading.hide();
                $state.go("begin");
            });

        }
    } 

})

.controller('eventsItemCtrl', function($scope, Util, $state, EndPoint, $ionicLoading, $stateParams, $ionicPopup) {
    $ionicLoading.show({template: 'Aguarde...'});
    $scope.event_id = $stateParams.event_id;

    $scope.occurrence = {};

    if(Util.getlocalStorage('user').type_profile=="student_account"){

        promise = EndPoint.getResource("v1/students/occurrences/"+$scope.event_id, {'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.occurrence     = response.data;
            //console.log(response.data)
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }else if(Util.getlocalStorage('user').type_profile=='parent_account'){

        promise = EndPoint.getResource("v1/parents/occurrences/"+$scope.event_id, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {
            $scope.occurrence     = response.data;
            $ionicLoading.hide();
        }, function(error) {
            Util.removelocalStorage('user');
            Util.removelocalStorage('headers');
            $ionicLoading.hide();
            $state.go("begin");
        });

    }
})



.controller('ChatsCtrl', function($scope, $timeout, $ionicLoading) {
})


.controller('ChatsCtrlView', function($scope, $state, $stateParams, $timeout, $ionicScrollDelegate, Util, EndPoint, $ionicLoading, ActionCableChannel, $ionicPopup) {
    $ionicLoading.show({template: 'Aguarde...'});

    $scope.user         = Util.getlocalStorage('user');
    $scope.type_user    = $stateParams.type_user;

    


    $scope.flag_sendMessage = true;
    var consumer;
    $scope.count = 0;

    $scope.user_school  = {};
    $scope.user_school  = {};
    $scope.chat_room    = {};
    
    $scope.data = {};
    
    $scope.showTime = true;
    $scope.messages = [];

    $scope.focusManager = { focusInputOnBlur: true };

    if($scope.user.type_profile=="student_account"){
        $scope.user.id      = parseInt($scope.user.id);

        promise = EndPoint.getResource("v1/students/user_schools/get_user_school/"+$scope.type_user, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {

            $scope.user_school = response.data;

            promise = EndPoint.postResource('v1/students/chat_rooms', {'type_user':$scope.type_user, 'user_school_id':$scope.user_school.id, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response2) {

                $scope.chat_room = response2.data;
                consumer = new ActionCableChannel("ChatChannel", {chat_room_id: $scope.chat_room.id});

                promise = EndPoint.getResource("v1/students/chat_messages/get_messages/"+$scope.user_school.id, {'type_user':$scope.type_user, 'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                promise.then(function(response3) {

                    //console.log(response3)
                    for(var i=0;i<response3.data.length;i++){
                        $scope.messages.push({
                            userId: parseInt(response3.data[i].sender_id),
                            text: response3.data[i].text,
                            avatar: response3.data[i].avatar,
                            time: response3.data[i].created_at
                        });
                    }
                    $ionicLoading.hide();
                    $ionicScrollDelegate.scrollBottom(true); 

                }, function(error3) {
                    $ionicLoading.hide();                    
                    $ionicPopup.alert({
                        title: 'Erro',
                        template: '<center>Ocorreu um erro, tente novamente</center>',
                        buttons: [{
                            text: 'ok',
                            type: 'button-positive',
                        }]
                    });
                    $state.go("tab.chats");
                    //Util.removelocalStorage('user');
                    //Util.removelocalStorage('headers');                   
                });



            }, function(error) {
                $ionicLoading.hide();                    
                $ionicPopup.alert({
                    title: 'Erro',
                    template: '<center>Ocorreu um erro, tente novamente</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $state.go("tab.chats");
                //Util.removelocalStorage('user');
                //Util.removelocalStorage('headers');                    
            });

        }, function(error2) {
            $ionicLoading.hide();                    
            $ionicPopup.alert({
                title: 'Erro',
                template: '<center>Ocorreu um erro, tente novamente</center>',
                buttons: [{
                    text: 'ok',
                    type: 'button-positive',
                }]
            });
            $state.go("tab.chats");
            //Util.removelocalStorage('user');
            //Util.removelocalStorage('headers');                    
        });

    }else if($scope.user.type_profile=='parent_account'){

        $scope.user.id      = parseInt($scope.user.data.id);
        
        promise = EndPoint.getResource("v1/parents/user_schools/get_user_school/"+$scope.type_user, {'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
        promise.then(function(response) {

            $scope.user_school = response.data;

            promise = EndPoint.postResource('v1/parents/chat_rooms', {'type_user':$scope.type_user, 'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
            promise.then(function(response2) {

                $scope.chat_room = response2.data;
                consumer = new ActionCableChannel("ChatChannel", {chat_room_id: $scope.chat_room.id});

                promise = EndPoint.getResource("v1/parents/chat_messages/get_messages", {'type_user':$scope.type_user, 'enrollment_student':Util.getlocalStorage('user').enrollment_student, 'client':Util.getlocalStorage('headers')['client'], 'access-token':Util.getlocalStorage('headers')['access-token'], 'uid':Util.getlocalStorage('headers')['uid']});
                promise.then(function(response3) {

                    //console.log(response3)
                    for(var i=0;i<response3.data.length;i++){
                        $scope.messages.push({
                            userId: parseInt(response3.data[i].sender_id),
                            avatar: response3.data[i].avatar,
                            text: response3.data[i].text,
                            time: response3.data[i].created_at
                        });
                    }
                    $ionicLoading.hide();

                }, function(error3) {
                    $ionicLoading.hide();                    
                    $ionicPopup.alert({
                        title: 'Erro',
                        template: '<center>Ocorreu um erro, tente novamente</center>',
                        buttons: [{
                            text: 'ok',
                            type: 'button-positive',
                        }]
                    });
                    $state.go("tab.chats");
                    //Util.removelocalStorage('user');
                    //Util.removelocalStorage('headers');                    
                });



            }, function(error) {
                $ionicLoading.hide();                    
                $ionicPopup.alert({
                    title: 'Erro',
                    template: '<center>Ocorreu um erro, tente novamente</center>',
                    buttons: [{
                        text: 'ok',
                        type: 'button-positive',
                    }]
                });
                $state.go("tab.chats");
                //Util.removelocalStorage('user');
                //Util.removelocalStorage('headers');                    
            });

        }, function(error2) {
            $ionicLoading.hide();                    
            $ionicPopup.alert({
                title: 'Erro',
                template: '<center>Ocorreu um erro, tente novamente</center>',
                buttons: [{
                    text: 'ok',
                    type: 'button-positive',
                }]
            });
            $state.go("tab.chats");
            //Util.removelocalStorage('user');
            //Util.removelocalStorage('headers');
        });


    }

    $scope.getItemHeight = function(event) {
        //console.log('getItemHeight', event);
        return 300;
    }

    $scope.sendMessage = function() {

        $scope.count++;
        $scope.flag_sendMessage = false;


        if($scope.user.type_profile=='student_account'){
            type_profile = 'StudentAccount'
            var data_obj = {
                message:$scope.data.message, 
                chat_room_id:$scope.chat_room.id, 
                school_id:$scope.user.school.id, 
                recipient_id:$scope.user_school.id, 
                recipient_type: 'UserSchool',
                sender_id:$scope.user.id, 
                sender_type:type_profile,
                type_user:$scope.type_user
            };
        }else if($scope.user.type_profile=='parent_account'){
            type_profile = 'ParentAccount'
            var data_obj = {
                message:$scope.data.message, 
                chat_room_id:$scope.chat_room.id, 
                school_id:$scope.user.school_id, 
                recipient_id:$scope.user_school.id, 
                recipient_type: 'UserSchool',
                sender_id:$scope.user.data.id, 
                sender_type:type_profile,
                type_user:$scope.type_user
            };
        }
        

        var callback = function(data){ 
            $scope.messages.push({
                userId: parseInt(data.message.sender_id),
                text: data.message.text,
                avatar: data.avatar,
                time: data.message.created_at
            });

            $scope.flag_sendMessage = true;
            delete $scope.data.message;
            $ionicScrollDelegate.scrollBottom(true);
        };  

        if($scope.count==1){
            consumer.subscribe(callback).then(function(){
                
                consumer.send(data_obj, 'send_message'); 
                //console.log(data_obj)
                
                $scope.$on("$destroy", function(){
                    consumer.unsubscribe().then(function(){ 
                        //$scope.sendToMyChannel = undefined; 
                    });
                });

            });
        }else{
            consumer.send(data_obj, 'send_message'); 
        }
    };


    $scope.inputUp = function() {
        if (isIOS) $scope.data.keyboardHeight = 216;
    };

    $scope.inputDown = function() {
        if (isIOS) $scope.data.keyboardHeight = 0;
    };

    $scope.closeKeyboard = function() {
        // cordova.plugins.Keyboard.close();
    };

    $scope.keepKeyboard = function() {
        //console.log('keepKeyboard');
    };

    var teste = function(){
        alert(zfsdf)
    }

})